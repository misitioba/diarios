import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.esm.browser.js'
import '/admin/common/globals.js'
import '/admin/common/fql.js'
import '/admin/common/settings.js'
import qsParams from '/admin/common/qs.js'
import resizableMixin from '/admin/mixins/resizable.js'

localforage.config({
    //driver      : localforage.WEBSQL, // Force WebSQL; same as using setDriver()
    name: 'vueapp',
    version: 1.0,
    size: 4980736, // Size of database, in bytes. WebSQL-only for now.
    //storeName   : 'keyvaluepairs', // Should be alphanumeric, with underscores.
    description: 'offline storage'
});

new Vue({
    mixins:[resizableMixin],
    el: '#app',
    watch: {
        "innerRightPopupStyle.width": function () {
            window.localforage.setItem('innerRightPopupStyle.width', this.innerRightPopupStyle.width)
        },
        section(){
            this.rightSidebar=''
            this.rightPopup=''
        }
    },
    data() {
        return {
            showSidebar:false,
            showApp:false,
            pagesFilters: {
                name:"",
                tags:"",
                isTemplate:false
            },
            showPagePreview:false,
            innerRightPopupStyle: {
                width: 100
            },
            rightPopup: '',
            selectedSection: {},
            rightSidebar: "",
            page: {
                sections: []
            },
            error: "",
            section:"login",
            lastSection: qsParams.get('section') || "",
            pages: [],
            dirty: false,
            serverErrors: []
        }
    },
    async created() {
        this.$root = this

        
        this.innerRightPopupStyle.width = (await window.localforage.getItem('innerRightPopupStyle.width')) || 100

        const socket = io();
        socket.on('error', ({ stack,date }) => {
            this.serverErrors.push(`${moment(date).format('HH:mm:ss')}
            ${stack}
            `)
        })
    },
    async mounted() {
        setTimeout(()=>{
            this.$nextTick(()=>{
                this.showApp=true
            })
        },100)
        await this.getPages()


        if (qsParams.get('page')) {
            this.editPage(this.pages.find(p => p.name == qsParams.get('page')) || {})
        }
        if (this.page && qsParams.get('pageSection')) {
            let section = this.page.sections.find(s => s.name == qsParams.get('pageSection'))
            if (section) {
                this.editSection(section)
            } else {
                qsParams.delete('pageSection')
            }
        }

        window.addEventListener("keyup", e => {
            if (e.key === 'Escape') {

                if(this.showPagePreview){
                    return this.backFromPagePreview()
                }

                if (this.rightPopup) {
                    this.rightPopup = ''
                    qsParams.delete('pageSection')
                    return
                }
                if (this.rightSidebar) {
                    this.rightSidebar = ''
                    qsParams.delete('page')
                    return
                }
                this.section = ''
                qsParams.delete('section')
            }
        });
    },
    computed: {
        canPreviewPage(){
            return this.page && this.page._id && !this.page.isTemplate
        },
        errorsHTML() {
            return this.serverErrors.reverse().join('<br/>')
        },
        rightPopupStyle() {
            let maxWidth = window.innerWidth * ((100 - this.innerRightPopupStyle.width) / 100)
            maxWidth = maxWidth < 230 ? 300 : maxWidth
            console.log('rightPopupStyle', {
                maxWidth
            })
            return {
                "max-width": `${maxWidth}px`
            }
        },
        aboutMessage() {
            return sectionValue('about', 'about_message')
        },
        customPages() {
            return this.pages.filter(p => !p.isCore).filter(p=>{
                let filters = this.pagesFilters
                let accept = true
                if(filters.name){
                    accept = p.name.indexOf(filters.name) !== -1
                }
                if(filters.isTemplate){
                    accept = accept && p.isTemplate
                }else{
                    accept = accept && !p.isTemplate
                }
                if(filters.tags){
                    filters.tags.split(' ').join(',').trim().split(',').forEach(t=>{
                        let tags = p.tags instanceof Array ?  p.tags.join(',') : p.tags
                        accept = accept && tags.indexOf(t)!==-1
                    })
                }
                return accept
            })
        },
        savePageDisabled() {
            return !this.page.name || (!this.page.path && !this.page.isTemplate)
        },
        uploadUrl() {
            return window.location.origin + '/funql-api?n=uploadSingleImage&multiparty=1'
        },
        getSectionUploadPostParams() {
            return { _funqlName: 'uploadSingleImage', pageId: this.page._id }
        },
    },
    methods: {
        onLoginSuccess(){
            this.section = this.lastSection || 'preview'
            this.showSidebar = true
        },
        themePreview(popupName){
            this.rightPopup=popupName
        },
        applyTemplate(){
            console.warn(this.$refs.rightPopupIdSelector)
            this.$refs.rightPopupIdSelector.open('getPageTemplatesForSelector', async _id=>{
              let templatePage = await this.$fql('getPage',[_id])
              this.page.name = this.page.name ? this.page.name : templatePage.name
              this.page.template = templatePage.template
              templatePage.sections.filter(s=>{
                  return !this.page.sections.find(ss=>ss.name == s.name)
              }).forEach(s=>{
                  let ss = {
                      ...s
                  }
                  delete ss._id
                  this.page.sections.push(ss)
              })
              templatePage.tags.filter(t=>{
                  let currentTags = this.page.tags
                  currentTags = currentTags instanceof Array ? currentTags : currentTags.split(',').map(ct=>ct.trim())
                  return !currentTags.find(tt=>tt==t)
              }).forEach(t=>{
                  if(typeof this.page.tags === 'string'){
                      this.page.tags+=','+t
                  }else{
                      this.page.tags.push(t)
                  }
              })
              await this.savePageInternal()
              await this.syncPage()
              this.editPage(this.page)
            })
        },
        normalizePagePath(key){
            eval(`this.${key} = this.${key}.split(' ').join('-').trim().toLowerCase()`)
        },
        normalizePageName(){
            this.page.name = this.page.name.split(' ').join('_').trim().toLowerCase()
        },
        closeSidebar() {
            this.rightSidebar = ''
            qsParams.delete('page')
        },
        getPageSectionValue(section) {
            if (section.type === 'address') {
                return section.value.formatted || ""
            }
            if (section.type == 'single_image') {
                return section.value.name
            }
            return section.value
        },
        editSection(section, isNew = false) {
            this.rightPopup = 'editSection'
            this.selectedSection = section
            if (!isNew) {
                this.$refs.sectionTitle.innerText = section.name
                qsParams.set('pageSection', section.name)
            } else {
                this.$refs.sectionTitle.innerText = 'new_section'
            }
        },
        addPageSection() {
            this.editSection({
                name: "new_section",
                type: "text",
                value: ""
            }, true)
        },
        openWebsite() {
            window.location.href = "/"
        },
        preview() {
            this.blur()
            this.reloadPreview()
            this.rightSidebar = ''
        },
        async getPages() {
            this.pages = await this.$fql('getPages')
        },
        async getPage(_id) {
            return await this.$fql('getPage',[_id])
        },
        async removePageSection() {
            if (!window.confirm('Sure?')) {
                return
            }
            await this.$fql('removePageSection', [this.page._id, this.selectedSection._id])
            this.closeSection();
            await this.syncPage(this.page._id)
            this.editPage(this.page)
        },
        async removePage() {
            if (!window.confirm('Sure?')) {
                return
            }
            await this.$fql('removePage', [{
                ...this.page
            }])
            this.rightSidebar = ''
            this.getPages()
        },
        onSectionName(e) {
            this.selectedSection.name = (e.target.innerText||"").toLowerCase()
            this.normalizeSectionName(e,500)
        },
        normalizeSectionName(e,timeout=0){
            if(timeout){
                if(this.normalizeSectionNameTimeout){
                    window.clearTimeout(this.normalizeSectionNameTimeout)
                }
                this.normalizeSectionNameTimeout = setTimeout(()=>this.normalizeSectionName(e),timeout)
                return
            }else{
                if(this.normalizeSectionNameTimeout){
                    window.clearTimeout(this.normalizeSectionNameTimeout)
                }
            }
            console.warn("normalizeSectionName")
            this.selectedSection.name= (e.target.innerText||"").split(' ').join('_').trim().toLowerCase()
            e.target.innerText = this.selectedSection.name
        },
        async savePageInternal() {
            try {
                this.error = ''
                await this.$fql('savePage', [{
                    ...this.page
                }])
                console.info('savePageInternal')
            } catch (err) {
                if (err.message === 'PATH_MISTMACH') {
                    this.error = "Tried to save page: The Path is already taken"
                }
                throw err
            }
        },
        closeSection() {
            qsParams.delete('pageSection')
            this.rightPopup = ""
        },
        cancelPage() {
            qsParams.delete('pageSection')
            qsParams.delete('page')
            this.rightPopup = ""
            this.rightSidebar = ''
        },
        async saveSection() {
            let nameMatch = this.page.sections.find(s => {
                let idCond = this.selectedSection._id===undefined ? true : (s._id != this.selectedSection._id)
                return  idCond && s.name == this.selectedSection.name
            })

            if (nameMatch) {
                return window.alert(`There is already an existing section with the same name (${this.selectedSection.name}). Try changing the name.`)
            }

            if (!this.selectedSection._id) {
                this.page.sections.push({
                    ...this.selectedSection
                })
            } else {
                let index = this.page.sections.findIndex(s => s.name == this.selectedSection.name)
                this.page.sections[index] = {
                    ...this.selectedSection
                }
            }
            await this.savePageInternal()
            qsParams.delete('pageSection')
            this.rightPopup = ""
            this.reloadPreview()
            await this.syncPage()
            this.editPage(this.page)
            this.reloadPagePreview()
        },
        async syncPage(){
            let page = await this.getPage(this.page._id)
            this.pages[this.pages.findIndex(p=>p.name==page.name)] = {
                ...this.pages[this.pages.findIndex(p=>p.name==page.name)],
                ...page
            }
            this.page = {
                ...this.page,
                ...page
            }
        },
        async savePage() {
            await this.savePageInternal()
            this.rightSidebar = ''
            this.reloadPreview()
            this.getPages()
        },
        backFromPagePreview(){
            this.rightSidebar='editPage'
            this.showPagePreview=false
        },
        previewPage(){
            this.showPagePreview=true
            //this.rightSidebar=""
            this.rightPopup=""
            this.reloadPagePreview()
        },
        reloadPagePreview(){
            this.$refs.pagePreview.refresh()
        },
        editPage(item) {
            this.rightSidebar = 'editPage',
                this.page = item
            qsParams.set('page', item.name)
        },
        addPage(item) {
            this.rightSidebar = 'editPage',
                this.page = {}
        },
        reloadPreview() {
            document.querySelector('iframe').src = document.querySelector('iframe').src
        },
        blur() {
            this.section = ''
            return true;
        },
        onChange() {
            this.dirty = true
        },
        async save() {
            await this.$fql('savePages', [this.pages.map(p => ({ ...p }))])
            this.dirty = false
            this.reloadPreview()
            this.blur()
        },
        sectionValue(page, section) {
            return this.pages.find(p => p.name == page) && this.pages.find(p => p.name == page).sections.find(s => s.name == section).value
        },
        onSectionTextUpdate(e, page, section) {
            this.pages.find(p => p.name == page).sections.find(s => s.name == section).value = e.target.value
            return true
        },
        goto(name) {
            this.section = name
            qsParams.set('section', this.section)
        }
    }
})

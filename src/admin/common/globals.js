import GeoAddress from '/admin/components/GeoAddress.js'
import ImageUpload from '/admin/components/ImageUpload.js'
import Themes from '/admin/components/Themes.js'
import PagePreview from '/admin/components/PagePreview.js'
import ThemeTemplateSelector from '/admin/components/ThemeTemplateSelector.js'
import PagesFilters from '/admin/components/PagesFilters.js'
import IdSelector from '/admin/components/IdSelector.js'
import RightPopupIdSelector from '/admin/components/RightPopupIdSelector.js'
import LoginSection from '/admin/components/LoginSection.js'
import {Vue} from '/admin/common/vue.js'
Vue.component('right-popup-id-selector', RightPopupIdSelector)
Vue.component('id-selector', IdSelector)
Vue.component('pages-filters', PagesFilters)
Vue.component('theme-template-selector', ThemeTemplateSelector)
Vue.component('themes', Themes)
Vue.component('page-preview', PagePreview)
Vue.component('geo-address', GeoAddress)
Vue.component('image-upload', ImageUpload)
Vue.component('login-section', LoginSection)
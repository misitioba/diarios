export default {
    props:{
        url:String,
        postParams: Object
    },
    template: `
    <div>
        <input ref="input" type="file" name="files[]" @change="change" />
        <div ref="root" class="drag-drop">    
            <div class="drag-files-label" v-text="file.name">
            </div>
            <a :href="src" target="_blank" v-show="src">
                <img class="image_upload__img" v-show="src" :src="src"/>    
            </a>
        </div>
        <button v-show="file.type && !uploaded" @click="upload">Upload</button>
    </div>`,
    data(){
        return {
            uploaded:false,
            src:"",
            file:{
                name: 'Drag your file right over here!',
                type:''
            }
        }
    },  
    mounted() {
        var root = this.$refs.root;
        var input = this.$refs.input;
        window.r = root;

        ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach(function (event) {
            root.addEventListener(event, function (e) {
                // preventing the unwanted behaviours
                e.preventDefault();
                e.stopPropagation();
            });
        });

        ['dragover', 'dragenter'].forEach(function (event) {
            root.addEventListener(event, function () {
                root.classList.add('dragover');
            });
        });

        ['dragleave', 'dragend', 'drop'].forEach(function (event) {
            root.addEventListener(event, function () {
                root.classList.remove('dragover');
            });
        });

        var droppedFiles;
        let self = this
        root.addEventListener('drop', function (e) {
            droppedFiles = e.dataTransfer.files; // the files that were dropped
            console.log('droppedFiles', {
                droppedFiles
            })
            if (droppedFiles.length > 0) {
                self.select( droppedFiles[0])
            }
        });
    },
    methods: {
        select(file){
            console.log(file)
            if(file.type.indexOf('image')===-1){
                return window.alert('Image expected')
            }
            this.file={
                name: file.name,
                filename: file.filename,
                type: file.type
            }
            this.fileBlob = new Blob([file])
            this.uploaded=false
        },
        change(e) {
            if(e.target.files.length>0){
                this.select(e.target.files[0])
            }
        },
        async upload(){
            const formData = new FormData();
            formData.append('file', this.fileBlob, this.file.name || this.file.filename);
            Object.keys(this.postParams).forEach(key=>{
                formData.append(key, this.postParams[key] )
            })
            formData.append('filename', this.file.name || this.file.filename)
            formData.append('type', this.file.type)
            let response = await fetch(this.url,
            {
                body: formData,
                method: "post"
            });
            let asset = await response.json()
            this.src = asset.publicPath
            this.uploaded=true
            this.$emit('input', asset || {})
        }
    }
}
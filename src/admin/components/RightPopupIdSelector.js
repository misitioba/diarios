export default {
    props:{
        visible:Boolean,
        rightPopupStyle:Object
    },
    template:`<div class="rightPopup" :style="rightPopupStyle" v-show="visible">
        <div class="rightPopup__content">
            <id-selector
            ref="idSelector"
            v-model="selectedValue"
            @input="change"
            :fqlRoute="fqlRoute"
            >
            </id-selector>
        </div>
    </div>`,
    data(){
        return {
            selectedValue:'',
            fqlRoute:''
        }
    },
    methods:{
        change(){
            if(this.selectedValue){
                this.callback && this.callback(this.selectedValue)
            }
            this.selectedValue = ''
            this.$emit('closed')
        },
        open(fqlRoute, callback){
            this.fqlRoute = fqlRoute
            this.$nextTick(()=>{
                this.callback = callback
                this.$emit('opened')
                this.$refs.idSelector.openTable()
            })
        }
    }
}
    export default{
    props:{
        value:String
    },
    template:`<div>
        <input :disabled="true" @click="openTable" :value="text" />
        <button @click="openTable">Set</button>
        <div v-show="showTable" class="pick_up_popup">
            <div v-for="item in items" :key="item" @click="select(item)" class="pick_up_popup__item">
                {{item}}
            </div>
        </div>
    </div>`,
    data(){
        return{
            text: this.value || "",
            showTable:false,
            items:[]
        }
    },
    watch:{
        value(){
            this.text=this.value
        }
    },
    methods:{
        async openTable(){
            this.showTable=true
            this.items = (await this.$fql('getThemeFiles',[ await this.$settings.getParam('theme') ])||[]).filter(path=>{
                return path.indexOf('.pug')!==-1
            })
        },
        select(item){
            item = item.split('.pug').join('')
            this.text = item
            this.$emit('input',item)
            this.showTable=false
        }
    }
}
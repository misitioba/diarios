export default {
    props: {
        value: Object
    },
    template: `<div ref="root" class="pages_filters">
        <div class="pages_filters__title">Filters</div>
        <label>Name</label>
        <input v-model="name" />
        <label>Tags</label>
        <input v-model="tags" />
        <div>
            <label>Template</label>
            <input type="checkbox" v-model="isTemplate" @change="change" />
        </div>
        <div class="pages_filters__add_to_favorites">
            <button @click="addToFavorites">Add to favorites</button>
        </div>
        <div class="pages_filters__list">
            <div v-for="(item) in predefinedFilters" class="pages_filters__list__item">
                <button  
                    v-text="item.label" 
                    @click="loadFilter(item)">
                </button>
                <button class="pages_filters__remove_icon" @click="removeFilter(item)">X</button>
            </div>
        </div>
    </div>`,
    data() {
        return {
            name: "",
            tags: "",
            isTemplate: false,
            predefinedFilters: []
        }
    },
    created() {
        this.syncFilters()
    },
    methods: {
        async syncFilters() {
            let param = await this.$settings.getParam('page_filters')
            this.predefinedFilters = param || []
        },
        change() {
            this.$emit('input', {
                name: this.name,
                tags: this.tags,
                isTemplate: this.isTemplate
            })
        },
        async removeFilter(item) {
            if(!window.confirm('Sure?')){
                return
            }
            let index = this.predefinedFilters.findIndex(i => i.name == item.name)
            if (index !== -1) {
                this.predefinedFilters.splice(index, 1)
                await this.saveFilters()
            }
        },
        async addToFavorites() {
            let label = window.prompt('Choose a label', "default")
            if(!label){
                return
            }
            await this.syncFilters()
            this.predefinedFilters.push({
                label,
                name: this.name,
                tags: this.tags,
                isTemplate: this.isTemplate
            })
            await this.saveFilters()
        },
        async saveFilters(){
            await this.$settings.saveParam({
                name: "page_filters",
                type: "object",
                value: this.predefinedFilters
            })
            await this.syncFilters()
        },
        loadFilter(item) {
            this.name = item.name
            this.tags = item.tags
            this.isTemplate = item.isTemplate
            this.change()
        }
    },
    watch: {
        name() {
            this.change()
        },
        tags() {
            this.change()
        }
    }
}
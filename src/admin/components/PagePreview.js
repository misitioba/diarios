import resizableMixin from '/admin/mixins/resizable.js'

export default {
    mixins:[resizableMixin],
    props: {
        visible: Boolean,
        path: String
    },
    template: `<div v-show="visible" class="page_preview" ref="root">
        <button @click="hide">Hide</button>    
        <button @click="sendToBack">Back</button>
        <button @click="sendToFront">Front</button>
        <button @click="resize(100)">100%</button>
        <button @click="resize(50)">50%</button>
        <iframe :src="getPath">
        </iframe>
        <div class="draggable_bar" @mousedown="resizeElementX($event,'root')"></div>
    </div>`,
    methods: {
        resize(percentage){
            this.$refs.root.style.width= `calc(${percentage||100}vw)`
        },
        sendToFront(){
            this.$refs.root.style.zIndex=15
        },
        sendToBack(){
            this.$refs.root.style.zIndex=5
        },
        hide(){
            this.sendToBack()
            this.$emit('back')
        },
        refresh() {
            this.$nextTick(()=>{
                this.$refs.root.querySelector('iframe').src = this.$refs.root.querySelector('iframe').src
            })
        }
    },
    computed:{
        getPath(){
            return this.visible ? this.path +'/?iframe=1' : '/?iframe=1'
        }
    }
}
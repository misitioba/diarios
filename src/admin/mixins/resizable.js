export default {
    data(){
        return {
            resizeData:{}
        }
    },
    methods:{
        resizeElementX(e, refName){
            e.preventDefault()
            let el = ()=>this.$refs[refName]
            this.resizeData[refName] = {
                lastX: e.clientX,
            }
            document.onmouseup = ()=>{
                document.onmouseup = null
                document.onmousemove = null
            }
            document.onmousemove = (e)=>{
                e.preventDefault()
                let diffX = this.resizeData[refName].lastX - e.clientX
                if(el()){
                    //el().style.left = el().offsetLeft - diffX + 'px'
                    let offsetLeft = e.clientX //el().offsetLeft - diffX;
                    el().style.maxWidth = `calc(100vw - ${offsetLeft}px)`
                    //console.warn('move',offsetLeft, e.clientX)
                }
                
            }
        }
    }
}